import {toast} from 'react-toastify';

let notifySuccess = (message)=>{
    toast.success(message)
}

let notifyError=(message)=>{
    toast.error(message)
}

export {notifyError, notifySuccess}
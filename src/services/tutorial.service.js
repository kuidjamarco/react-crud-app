import http from "../httpCommon";

function getAllTutorials() {
    return http.get("/tutorials");
}

function getTutorial(id) {
    return http.get(`/tutorials/${id}`);
}

function createTutorial(data) {
    return http.post("/tutorials", data);
}

function updateTutorial(data) {
    return http.put(`/tutorials/${data.id}`, data);
}

function deleteTutorial(id) {
    return http.delete(`/tutorials/${id}`);
}

function deleteAllTutorials() {
    return http.delete(`/tutorials`);
}

function searchTutorialsFromBackEnd(title){
    return http.get(`tutorials/search/${title}`);
}

export {
    createTutorial, 
    getAllTutorials, 
    getTutorial,
    updateTutorial, 
    deleteTutorial, 
    deleteAllTutorials,
    searchTutorialsFromBackEnd
};
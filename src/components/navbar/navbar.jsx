import { useContext } from 'react';
import { Link } from 'react-router-dom';
import {makeStyles, Box, } from '@material-ui/core';
import TutorialsContext from '../contexts/tutorialsContext/tutorials.context';
import CurrentPageContext from '../contexts/currentPageContext/currentPage.context';

const useStyle = makeStyles(theme => ({
    Navbar:{
        display:'flex',
        backgroundColor:theme.palette.primary.main,
        padding:'10px',
    },
    Links:{
        color:'black',
        marginInline:'20px',
        fontSize:'20px',
        textDecoration:'none'
    },
    Active:{
        color:'white',
    }
}));

function Navbar(props){
    let classes = useStyle();

    let currentPageContext = useContext(CurrentPageContext);
    let {currentPage} = currentPageContext;

    let tutorialsContext = useContext(TutorialsContext)
    let {selectedTutorial} = tutorialsContext;

    return(
        <Box className={classes.Navbar}>
            <Link 
                to='/tutorials' 
                className={currentPage === 'tutorials' ? classes.Links+" "+ classes.Active : classes.Links}
            >
                MyTutorials
            </Link>
            <Link 
                to='/add' 
                className={currentPage === 'add' ? classes.Links+" "+ classes.Active : classes.Links}
            >
                Add Tutorial
            </Link>
            { selectedTutorial.id ? <Link 
                to={`tutorialHandle/${selectedTutorial.id}`}
                className={currentPage === 'tutorialHandle' ? classes.Links+" "+ classes.Active : classes.Links}
            >
                Handle Tutorial
            </Link>: null }
        </Box>
    )
}

export default Navbar;
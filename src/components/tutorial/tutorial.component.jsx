import * as yup from 'yup';
import { useFormik } from 'formik';
import { withRouter } from 'react-router';
import { useContext, useEffect } from 'react';
import { notifySuccess } from '../../utils/toastMessages';
import TutorialsContext from '../contexts/tutorialsContext/tutorials.context';
import { updateTutorial, deleteTutorial, } from '../../services/tutorial.service';
import CurrentPageContext from '../contexts/currentPageContext/currentPage.context';
import IsSubmittingContext from '../contexts/isSubmittingContext/isSubmitting.context';
import {Grid, Typography, TextField, Button, makeStyles , InputLabel, Box, CircularProgress } from '@material-ui/core';

const useStyle = makeStyles(theme => ({
    Body:{
        padding:'30px',
        display:'grid'
    },
    Title:{
        textAlign:'left',
    },
    Buttons:{
        display:'grid',
        gridAutoFlow:'column',
        justifyContent:"center",
        padding:'20px',
    },
    Input:{
        textAlign:'left',
        padding:'20px'
    },
    Button:{
        marginInline:"20px",
        paddingInline:'40px',
    },
}))

function Tutorial(props){
    let classes = useStyle();

    let currentPageContext = useContext(CurrentPageContext)
    let {setCurrentPage} = currentPageContext;
    

    let tutorialsContext = useContext(TutorialsContext);
    let {selectedTutorial, setSelectedTutorial} = tutorialsContext;

    let isSubmittingContext = useContext(IsSubmittingContext)
    let {isSubmitting, setIsSubmitting} = isSubmittingContext;

    const publishTutorialHandler = () =>{
        console.log("publishTutorialHandler")
        setIsSubmitting(true);
        let newSelected = selectedTutorial

        newSelected = {...newSelected, published:!selectedTutorial.published}
        console.log(newSelected)
        updateTutorial(newSelected)
        .then(response=>{
            setSelectedTutorial(newSelected)
            notifySuccess(response.data.message)
            setIsSubmitting(false);
        })
        .catch(error=>{
            console.log(error)
            setIsSubmitting(false);
        })

    }

    const deleteTutorialHandler = () => {
        console.log("deleteTutorialHandler")
        setIsSubmitting(true);
        deleteTutorial(selectedTutorial.id)
        .then(response => {
            notifySuccess(response.data.message)
            setSelectedTutorial({});
            props.history.push('/tutorials')
            setIsSubmitting(false);
        })
        .catch(error => {
            console.log(error)
            setIsSubmitting(false);
        })
    }

    const validationSchema = yup.object({
        title: yup
            .string("Type tutorial title ")
            .required("Toturial title is required"),
        description: yup
            .string("Tutorial description")
            .required("Toturial description is required")
            .notOneOf([yup.ref('title'), null], "Title and description cannot be thesame")
    })

    const formik = useFormik({
        initialValues:{
            title: selectedTutorial.title || '',
            description: selectedTutorial.description || ''
        },
        validationSchema:validationSchema,
        onSubmit: (values) => {
            setIsSubmitting(true);
            console.log("updateTutorialHandler")
            let newSelected = {...selectedTutorial, ...values};
            updateTutorial(newSelected)
            .then(response => {
                setSelectedTutorial(newSelected);
                console.log(selectedTutorial)
                notifySuccess(response.data.message)
                setIsSubmitting(false);
            })
            .catch(error => {
                console.log(error)
                setIsSubmitting(false);
            })

        }
    })

    useEffect(() =>{
        setCurrentPage('tutorialHandle');
    }, [])

    return (
        <Box className={classes.Body}>
            <Grid container direction='row' justify='center' alignItems='center' style={{padding:'50px'}}>
                <Grid item xs={10} sm={10}>
                    <Typography variant='h4' className={classes.Title}>Tutorial</Typography>
                </Grid>
                <Grid item xs={10} sm={10} style={{paddingTop:'10px'}}>
                    <form onSubmit={formik.handleSubmit}>
                        <Box className={classes.Input}>
                            <InputLabel htmlFor='title' style={{fontSize:'20px',}}>Title</InputLabel>
                            <TextField
                                id='title'
                                name='title'
                                placeholder='Tutorial title'
                                variant='outlined'
                                value = {formik.values.title || ''}
                                onChange={formik.handleChange}
                                style={{width:'100%', fontSize:'20px',}}
                                error={formik.touched.title && Boolean(formik.errors.title)}
                                helperText={formik.touched.title && formik.errors.title}
                            />
                        </Box>
                        <Box className={classes.Input} >
                            <InputLabel htmlFor='title' style={{fontSize:'20px',}}>Description</InputLabel>
                            <TextField
                                id='description'
                                name='description'
                                placeholder='Tutorial description'
                                variant='outlined'
                                value={formik.values.description || ''}
                                onChange={formik.handleChange}
                                style={{width:'100%', fontSize:'20px',}}
                                error={formik.touched.description && Boolean(formik.errors.description)}
                                helperText={formik.touched.description && formik.errors.description}
                            />
                        </Box>
                        <Box className={classes.Buttons}>
                            <Button 
                                onClick={() => publishTutorialHandler()} 
                                className={classes.Button}
                                variant='contained'
                                color='primary'
                            >
                                {isSubmitting && <CircularProgress color='secondary' style={{marginRight:"10px"}} size={20} />}
                                {selectedTutorial.published ? "Unpublish" : "Publish"}
                            </Button>
                            <Button
                                onClick={() => deleteTutorialHandler()} 
                                className={classes.Button}
                                variant='contained'
                                color='secondary'
                            >
                                {isSubmitting && <CircularProgress color='primary' style={{marginRight:"10px"}} size={20} />}
                                Delete
                            </Button>
                            <Button 
                                type='submit' 
                                onClick={formik.handleSubmit} 
                                className={classes.Button}
                                variant='contained'
                                color='primary'
                                disabled = {
                                    selectedTutorial.title === formik.values.title && selectedTutorial.description === formik.values.description
                                }
                            >
                                {isSubmitting && <CircularProgress color='secondary' style={{marginRight:"10px"}} size={20} />}
                                Update
                            </Button>
                        </Box>
                    </form>
                </Grid>
            </Grid>
        </Box>
    );
}

export default withRouter(Tutorial);
import * as yup from 'yup';
import { useContext, useEffect } from 'react';
import { useFormik } from 'formik';
import { notifySuccess } from '../../utils/toastMessages';
import { createTutorial } from '../../services/tutorial.service';
import TutorialsContext from "../contexts/tutorialsContext/tutorials.context";
import CurrentPageContext from '../contexts/currentPageContext/currentPage.context';
import IsSubmittingContext from '../contexts/isSubmittingContext/isSubmitting.context';
import {Grid, Typography, TextField, Button, makeStyles , InputLabel, Box, CircularProgress } from '@material-ui/core';

const useStyle = makeStyles(theme => ({
    Body:{
        padding:'30px',
        display:'grid'
    },
    Title:{
        textAlign:'left',
    },
    Buttons:{
        display:'grid',
        gridAutoFlow:'column',
        justifyContent:"center",
        padding:'20px',
    },
    Input:{
        textAlign:'left',
        padding:'20px'
    },
    Save:{
        marginInline:"20px",
        paddingInline:'50px',
    }
}))

function Tutorial(){
    let classes = useStyle();

    let currentPageContext = useContext(CurrentPageContext)
    let {setCurrentPage} = currentPageContext;
    
    let tutorialsContext = useContext(TutorialsContext)
    let {tutorials, setTutorials} = tutorialsContext;

    let isSubmittingContext = useContext(IsSubmittingContext)
    let {isSubmitting, setIsSubmitting} = isSubmittingContext

    const validationSchema = yup.object({
        title: yup
            .string("Type tutorial title ")
            .required("Toturial title is required"),
        description: yup
            .string("Tutorial description")
            .notOneOf([yup.ref('title'), null], "Title and description cannot be thesame")
            .required("Tutorial description is required"),
    })

    const formik = useFormik({
        initialValues:{
            title:'',
            description: ''
        },
        validationSchema:validationSchema,
        onSubmit: (values, {resetForm}) => {
            console.log("addTutorialHandler")
            setIsSubmitting(true)
            let newTutorial = {
                title:values.title, 
                description:values.description, 
                status:false 
            };
            createTutorial(newTutorial)
            .then( response =>{
                resetForm()
                setIsSubmitting(false);
                setTutorials([...tutorials, newTutorial]);
                notifySuccess(response.data.title+ " was added successfully")
            })
            .catch(error => {
                console.log(error)
                setIsSubmitting(false);
            })
        }
    })

    useEffect(() =>{
        setCurrentPage('add');
    }, [])

    return (
        <Box className={classes.Body}>
            <Grid container direction='row' justify='center' alignItems='center' style={{padding:'50px'}}>
                <Grid item xs={10} sm={10}>
                    <Typography variant='h4' className={classes.Title}>Add new tutorial</Typography>
                </Grid>
                <Grid item xs={10} sm={10} style={{padding:'10px', textAlign:'right'}}>
                    <form onSubmit={formik.handleSubmit}>
                        <Box className={classes.Input}>
                            <InputLabel htmlFor='title' style={{fontSize:'20px',}}>Title</InputLabel>
                            <TextField
                                id='title'
                                name='title'
                                placeholder='Tutorial title'
                                variant='outlined'
                                value={formik.values.title || ''}
                                onChange={formik.handleChange}
                                style={{width:'100%', fontSize:'20px',}}
                                error={formik.touched.title && Boolean(formik.errors.title)}
                                helperText={formik.touched.title && formik.errors.title}
                            />
                        </Box>
                        <Box className={classes.Input} >
                            <InputLabel htmlFor='title' style={{fontSize:'20px',}}>Description</InputLabel>
                            <TextField
                                id='description'
                                name='description'
                                placeholder='Tutorial description'
                                variant='outlined'
                                value={formik.values.description || ''}
                                onChange={formik.handleChange}
                                style={{width:'100%', fontSize:'20px',}}
                                error={formik.touched.description && Boolean(formik.errors.description)}
                                helperText={formik.touched.description && formik.errors.description}
                            />
                        </Box>
                        <Button 
                            type="submit" 
                            onClick={formik.handleSubmit} 
                            className={classes.Save}
                            disabled={isSubmitting}
                            variant='contained'
                            color='primary'
                        >
                            {isSubmitting && <CircularProgress color='secondary' style={{marginRight:"10px"}} size={25} />}
                            Save
                        </Button>
                    </form>
                </Grid>
            </Grid>
        </Box>
    );
}

export default Tutorial;
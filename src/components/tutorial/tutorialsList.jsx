import * as yup from 'yup'
import Hoc from '../../hoc/hoc';
import  { useFormik } from 'formik';
import { withRouter } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import {Scrollbars} from 'react-custom-scrollbars-2';
import { notifyError, notifySuccess} from '../../utils/toastMessages'
import TutorialsContext from '../contexts/tutorialsContext/tutorials.context';
import CurrentPageContext from '../contexts/currentPageContext/currentPage.context';
import { deleteAllTutorials, getAllTutorials, searchTutorialsFromBackEnd } from '../../services/tutorial.service';
import IsSubmittingContext from '../contexts/isSubmittingContext/isSubmitting.context';
import { Grid, Box, makeStyles, Divider, TextField, Button, Typography, CircularProgress, Select, MenuItem } from '@material-ui/core';

const maxSize = 10;

const useStyle = makeStyles((theme) =>({
    Body:{
        flexGrow: 1,
        padding:'50px',
        display:'grid'
    },
    Paper:{
        paddingInline: '3vw',
        textAlign:'left',
        color:theme.palette.text.secondary
    },
    SearchTextField:{
        width:"60vw",
        
    },
    Tuto:{
        textAlign:'left',
        paddingInline:'50px'
    },
    Selected:{
        backgroundColor:theme.palette.primary.main
    },
    Titles:{
        cursor:'pointer',
    },
    Modify:{
        backgroundColor:theme.palette.primary.light,
        marginInline:"20px",
        color:'white',
        '&:hover':{
            color:'black',
        }
    }
}));

function TutoList(props){
    let classes = useStyle();

    let currentPageContext = useContext(CurrentPageContext)
    let {setCurrentPage} = currentPageContext;

    let tutorialsContext = useContext(TutorialsContext)
    let { tutorials, setTutorials, selectedTutorial, setSelectedTutorial } = tutorialsContext; 
    
    let isSubmittingContext = useContext(IsSubmittingContext)
    let {isSubmitting, setIsSubmitting } = isSubmittingContext;

    let [noFoundTutorialMessage , setNoFoundTutorialMessage] = useState(null)
    
    const loadTutorials = (tutorialsFromDataBase) => {
        if(tutorialsFromDataBase.length === 0){
            setTutorials(tutorialsFromDataBase)
            setNoFoundTutorialMessage('No tutorial was found with the given keyword');
        }else if(tutorialsFromDataBase.length <= maxSize){
            setTutorials(tutorialsFromDataBase)
            setNoFoundTutorialMessage(null);
        }else{
            let newResult = []
            for(let i = 0; i < maxSize; i++)
                newResult[i] = tutorialsFromDataBase[i];
            setTutorials(newResult)
            setNoFoundTutorialMessage(null);
        }
    }

    const deleteAllTutorialsHandler = () => {
        setIsSubmitting(true)
        deleteAllTutorials()
        .then(response => {
            setTutorials([])
            notifySuccess(response.data.message)
            setIsSubmitting(false);
        })
        .catch(error => {
            notifyError(error.message || "Sorry some thing went wrog")
            setIsSubmitting(false)
        })
    }

    const searchTutorialHandler = (event) => {
        let searchTutorials = tutorials.filter(tutorial => tutorial.title.indexOf(event.target.value) >= 0);
        let oldTutorials = tutorials.filter(tutorial => tutorial.title.indexOf(event.target.value) < 0);
        if(searchTutorials.length === 0)
            setNoFoundTutorialMessage('No tutorial was found with the given keyword');
        else{
            setTutorials([...searchTutorials, ...oldTutorials])
            setSelectedTutorial({...selectedTutorial, ...searchTutorials[0]})
            setNoFoundTutorialMessage(null);
        }
        
    }

    const validationSchema = yup.object({
        search: yup
            .string("Enter your keyword")
            .trim('only letter are permitted')
            .required('a keyword is required for deep search')
    })
    const formik = useFormik({
        initialValues:{
            search:''
        },
        validationSchema:validationSchema,
        onSubmit: (values) => {
            searchTutorialsFromBackEnd(values.search)
            .then(response => {
                loadTutorials(response.data)
            })
            .catch(error => {
                console.log(error.message)
            })
        }
    })
        
    const selectTutorialHandler = (tutorial) =>{
        setSelectedTutorial(tutorial);
    }

    const gotoTutorialHandler = () => {
        props.history.push(`/tutorialHandle/${selectedTutorial.id}`);
    }

    let [searchOption, setSearchOption] = useState('local')
    const searchOptionHandler = (event) =>{
        setSearchOption(event.target.value)
    }

    const deepSearchHandler = (event) =>{
        if(event.target.value){
            searchTutorialsFromBackEnd(event.target.value)
            .then(response => {
                loadTutorials(response.data)
            })
            .catch(error => {
                console.log(error.message)
            })
        }
            
    } 

    useEffect(() =>{
        setCurrentPage('tutorials');
        getAllTutorials()
        .then(response => {
            loadTutorials(response.data)
        }).catch(error =>{
            notifyError(error.message || "Sorry some thing went wrong, try again later")
        })
    }, [])

    return (
        <Box className={classes.Body}>
            <Grid container spacing={3} alignItems="center">
                <Grid item xs={10} sm={12}>
                    <Box style={{display:'grid', justifyContent:"center"}}>
                        <Select
                            label='search'
                            id='search'
                            value={searchOption}
                            onChange={searchOptionHandler}
                            style={{height:'100%', fontSize:'20px'}}
                        >
                            <MenuItem value='local'>search</MenuItem>
                            <MenuItem value='deep'>Deep search</MenuItem>
                        </Select>
                        <form onSubmit={formik.handleSubmit}>
                            <TextField 
                                id='search'
                                name='search'
                                placeholder='Type tutorial title'
                                variant='outlined'
                                className={classes.SearchTextField}
                                onChange={searchOption === 'local' ? searchTutorialHandler : deepSearchHandler}
                            />
                        </form>
                        {searchOption === 'local' && noFoundTutorialMessage ? noFoundTutorialMessage : null}
                    </Box>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Typography variant='h3' className={classes.Paper}>
                        Tutoria List
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Typography variant='h3' className={classes.Paper}>
                        Tutorial
                    </Typography>
                </Grid>
                <Grid item xs={10} sm={6} style={{display:'grid', paddingLeft:'5px'}}>
                    <Scrollbars style={{width:'100%', height:'200px'}}>
                        <Box className={classes.Tuto} >
                            {tutorials.length > 0 ? tutorials.map((tutorial, index) =>
                                <Hoc key={index}>
                                    <Typography 
                                        variant='body1'
                                        style={{'&:hover':{backgroundColor:''}}} 
                                        className={selectedTutorial.id === tutorial.id ? classes.Selected : classes.Titles} 
                                        onClick={() => selectTutorialHandler(tutorial)}
                                    >
                                        {tutorial.title}
                                    </Typography>
                                    <Divider />
                                </Hoc>
                            ): <Typography variant='body2'>
                                {noFoundTutorialMessage || "Not tutorial yet"}
                            </Typography> }
                        </Box>
                    </Scrollbars>
                    <Button
                      onClick={deleteAllTutorialsHandler}
                      disabled ={tutorials.length === 0 || isSubmitting}
                      color="secondary"
                      variant="contained"
                      style={{width:'160px'}}
                    >
                        {isSubmitting && <CircularProgress color='primary' style={{marginRight:"10px"}} size={25} />}
                        Remove All
                    </Button>
                </Grid>
                <Grid item xs={6} sm={6}>
                    <Box className={classes.Tuto}>
                        <Box style={{display:'flex', fontSize:'25px',}}>
                            Title: 
                            <Typography variant='h6' color='primary'>
                                {selectedTutorial.title || ''}
                            </Typography>
                        </Box>
                        <Typography variant='h6'>
                            Description: {selectedTutorial.description || ''}
                        </Typography>
                        <Box style={{display:'flex', fontSize:'25px',}}>
                            Published: 
                            <Typography variant='h6' color='secondary'>
                                {(selectedTutorial.published ? "true": (selectedTutorial.published === undefined? "":"false"))}
                            </Typography>
                        </Box>
                    </Box>
                    <Box style={{textAlign:'right', padding:'10px'}}>
                        <Button
                          onClick={gotoTutorialHandler}
                          color="primary"
                          variant="contained"
                          disabled={tutorials.length === 0 || selectedTutorial.id === undefined || isSubmitting}
                        >
                          Modify
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
}

export default withRouter(TutoList);
import {useState} from 'react';
import TutorialsContext from "./tutorials.context";

function TutorialContextProvider({children}){
    let [tutorials, setTutorials] = useState([]);
    let [selectedTutorial, setSelectedTutorial] = useState({});

    let value = {tutorials, setTutorials, selectedTutorial, setSelectedTutorial};

    return(
        <TutorialsContext.Provider value={value}>
            {children}
        </TutorialsContext.Provider>
    )
}

export default TutorialContextProvider;
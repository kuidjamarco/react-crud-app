import { useState } from 'react';
import IsSubmittingContext from "./isSubmitting.context";

function IsSubmittingContextProvider({children}){
    let [isSubmitting, setIsSubmitting] = useState(false);

    let value = {isSubmitting, setIsSubmitting };

    return (
        <IsSubmittingContext.Provider value = {value}>
            {children}
        </IsSubmittingContext.Provider>
    )
}

export default IsSubmittingContextProvider;
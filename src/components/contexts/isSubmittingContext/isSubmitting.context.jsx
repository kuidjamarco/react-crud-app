import { createContext } from "react";
const IsSubmittingContext = createContext();
export default IsSubmittingContext;
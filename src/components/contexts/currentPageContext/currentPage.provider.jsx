import { useState } from "react";
import CurrentPageContext from "./currentPage.context";

function CurrentPageContextProvider({children}){
    let [currentPage, setCurrentPage] = useState('tutorials')
    let value = {currentPage, setCurrentPage};

    return(
        <CurrentPageContext.Provider value = {value}>
            {children}
        </CurrentPageContext.Provider>
    )
}

export default CurrentPageContextProvider;
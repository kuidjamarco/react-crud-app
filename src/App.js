import 'react-toastify/dist/ReactToastify.css';
import {Switch, Route} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import Navbar from './components/navbar/navbar';
import { BrowserRouter } from 'react-router-dom';
import TutoList from './components/tutorial/tutorialsList';
import Tutorial from './components/tutorial/tutorial.component';
import AddTutorial from './components/tutorial/addTutorial.component';
import TutorialContextProvider from './components/contexts/tutorialsContext/tutorials.provider';
import IsSubmittingContextProvider from './components/contexts/isSubmittingContext/isSubmitting.provider';
import CurrentPageContextProvider from './components/contexts/currentPageContext/currentPage.provider';

function App() {

  return (
    <BrowserRouter>
      <CurrentPageContextProvider>
        <TutorialContextProvider>
          <ToastContainer/>
          <Navbar />
          <IsSubmittingContextProvider>
            <Switch>
              <Route exact path={["/", "/tutorials", "/search"]} render={() => <TutoList />} />
              <Route exact path="/add" render={() => <AddTutorial />} />
              <Route path="/tutorialHandle" render={() => <Tutorial />} />
            </Switch>
          </IsSubmittingContextProvider>
        </TutorialContextProvider>
      </CurrentPageContextProvider>
    </BrowserRouter>
  );
}

export default App;